/*
 * GamePlay.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package EchoBerry;

import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.FullScreen;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.Alert;
import java.lang.Thread;
import java.lang.Math;
import java.util.*;
import java.io.*;



/**
 * 
 */
class GamePlay extends FullScreen {
    
    Refresher _refresher;  // Thread that continually refreshes the game
    boolean _active; // Flag if our game is currently active or not (eg did we lose)
    int _maxwidth = 0;
    int _maxheight = 0;
    int _rectx = 10;
    
    Bitmap _bluenorm;
    Bitmap _bluehigh;
    Bitmap _rednorm;
    Bitmap _redhigh;
    Bitmap _yellownorm;
    Bitmap _yellowhigh;
    Bitmap _greennorm;
    Bitmap _greenhigh;    
    Bitmap _gameover;
    Bitmap _tkey;
    Bitmap _gkey;
    Bitmap _ukey;
    Bitmap _jkey;
    
    int _blockwidth;
    int _blockheight;

    private SimonThread _simon;
    
    private boolean _soundstop = false;
    private boolean _soundplaying = false;
    private static Random _rnd = new Random();
    private int[] _levels = new int[100];
    private int _userlevel = 0;
    private int _copylevel = 0;
    private int _userscore = 0;
    private boolean _showkeys = true;
    private boolean _usesound = true;

    boolean getActive() { return _active; }

    char _lastkey;
    int _lastkeystatus;
    String _lastaction;
    boolean _lightkey = false;
    int _maxkeytime = 8;
    int _keytime = 0;
    int _looptime = 50;
    boolean _userlost = false;
    boolean _init = true;     
   
    GamePlay(UserSettings usersettings) {    
        int a = 0;
        _init = true;
        
        _active = true;
                
        _maxheight = Display.getHeight();
        _maxwidth = Display.getWidth();
        
        if (_maxheight > 320) {
            _maxheight = 320;
        }
        
        // Check the settings
        if (usersettings._usekeyprompt.compareTo("yes") == 0) {
            _showkeys = true;
        } else {
            _showkeys = false;
        }
        
        if (usersettings._usesound.compareTo("yes") == 0) {
            _usesound = true;
        } else {
            _usesound = false;
        }
              
        // Set up the block width and height for these images
        _blockwidth = 235;
        _blockheight = 155;
    
        // Load the images from the resources
        _bluenorm = Bitmap.getBitmapResource("BlueNorm-480.png");
        _bluehigh = Bitmap.getBitmapResource("BlueHigh-480.png");
        _rednorm = Bitmap.getBitmapResource("RedNorm-480.png");
        _redhigh = Bitmap.getBitmapResource("RedHigh-480.png");
        _yellownorm = Bitmap.getBitmapResource("YellowNorm-480.png");
        _yellowhigh = Bitmap.getBitmapResource("YellowHigh-480.png");
        _greennorm = Bitmap.getBitmapResource("GreenNorm-480.png");
        _greenhigh = Bitmap.getBitmapResource("GreenHigh-480.png");
        _gameover = Bitmap.getBitmapResource("gameover.png");
        
        _tkey = Bitmap.getBitmapResource("T-key.png");
        _gkey = Bitmap.getBitmapResource("G-key.png");
        _ukey = Bitmap.getBitmapResource("U-key.png");
        _jkey = Bitmap.getBitmapResource("J-key.png");
    
        // Generate the random sequences
        for (a=0;a<100;a++) {
            _levels[a] = GetRand(4);
        }
         
    
        _refresher = new Refresher();
        
        _simon = new SimonThread();
        _userlevel++;
    }
    
        
    // Refresh is a type of thread that runs to refresh the game.
    // Its job is to make sure all the processing is called for each object, update the background,
    // update score, check for end of game, etc.  This is the main heartbeat.
    private class Refresher extends Thread
    {
        // When the object is created it starts itself as a thread
        Refresher()
        {
           start();
        }
 
        // This method defines what this thread does every time it runs.
        public void run()
        {            
            // This thread runs while the game is active
            while (_active)
            {
 
                // Now that all our processing is done, tell the graphics engine to
                // redraw the screen through a call to invalidate (invalidates the screen
                // and automatically causes a redraw)
                invalidate();

                try
                {
                    // Attempt to sleep for 50 ms
                    this.sleep(_looptime);
 
                }
                catch (InterruptedException e)
                {
                    // Do nothing if we couldn't sleep, we don't care about exactly perfect
                    // timing.
                }
            }
        }
    }
        
    private class SimonThread extends Thread
    {
        // When the object is created it starts itself as a thread
        int s = 0;
        
        SimonThread()
        {
           start();
        }
 
        // This method defines what this thread does every time it runs.
        public void run()
        {            
                // This thread runs while the game is active
            
 
                // Now that all our processing is done, tell the graphics engine to
                // redraw the screen through a call to invalidate (invalidates the screen
                // and automatically causes a redraw)
                if (_init == true) {
                    _init = false;
                try
                {
                    // Attempt to sleep for 50 ms
                    this.sleep(1500);

                }
                catch (InterruptedException e)
                { }
                }
                
                try
                {
                    // Attempt to sleep for 50 ms
                    this.sleep(1500);

                }
                catch (InterruptedException e)
                { }
                
                
                for (s = 0;s<_userlevel;s++) {
                    
                    if (_levels[s] == 0) {
                        _lastaction = "blue";
                    }
                    
                    if (_levels[s] == 1) {
                        _lastaction = "red";
                    }
                    
                    if (_levels[s] == 2) {
                        _lastaction = "yellow";
                    }
                    
                    if (_levels[s] == 3) {
                        _lastaction = "green";
                    }

                    
                    _lightkey = true;
                    //StopSound();
                    PlaySound(_lastaction);

                    try
                    {
                        // Attempt to sleep for 50 ms
                        this.sleep((_looptime * _maxkeytime) + 200);
    
                    }
                    catch (InterruptedException e)
                    { }
                
                }
                
                _copylevel = 0;
                _maxkeytime = 6;
            
        }
    }
        
    public static  int GetRand (int Max)
    {
        return _rnd.nextInt(Max);
    }
        
    public void validateEntry(String strColour)
    {
        int entry = 0;
        if (strColour.compareTo("blue") == 0) {
            entry = 0;
        }
        
        if (strColour.compareTo("red") == 0) {
            entry = 1;
        }
        
        if (strColour.compareTo("yellow") == 0) {
            entry = 2;
        }
        
        if (strColour.compareTo("green") == 0) {
            entry = 3;
        }
        
        if (entry == _levels[_copylevel]) {
            // User got it right!
            _copylevel++;
            
            if (_copylevel == _userlevel) {
                _userscore = _userlevel;
                _maxkeytime = 8;
                
                if (_userlevel >=7 && _userlevel <=10) {
                    _maxkeytime = 5;
                }
                
                if (_userlevel >=11 && _userlevel <=14) {
                    _maxkeytime = 4;
                }
                
                if (_userlevel > 14) {
                    _maxkeytime = 4;
                }                
                
                _simon = new SimonThread();
                _userlevel++;
            }
        } else {
            // Nop user got it wrong
            _userlost = true;
            PlaySound("lost");
        }
    }
        
    public void PlaySound (String strColour)
    {
        //_soundstop = false;
        //_soundthread = new SoundThread(strColour);
        //_soundthread.start();        
        //_soundplaying = true;
        //boolean usesound = true;
        
        if (_usesound == true) {
            short[] fire = new short[2];
            short c = 16*2;
            short d = 18;
            short e = 20;
            short g = 24;
            
            if (strColour.compareTo("lost") == 0) {
                fire[0] = 500;            
            }
                    
                    
            if (strColour.compareTo("blue") == 0) {
                fire[0] = (short) (c*35);            
            }
            
            if (strColour.compareTo("red") == 0) {
                fire[0] = (short) (d*35);  
            }
            
            if (strColour.compareTo("yellow") == 0) {
                fire[0] = (short) (e*35);  
            }
            
            if (strColour.compareTo("green") == 0) {
                fire[0] = (short) (g*35);  
            }
            
            if (strColour.compareTo("lost") == 0) {                
                fire[1] = (short) (1500);
            } else {
                fire[1] = (short) (_looptime * _maxkeytime);
            }
            
            
            //fire = {1400, 15};
    
            try
            {
                Alert.startAudio(fire, 100);
    
            }
            catch (Exception ex)
            {
            
            }
        }

    }
    
                
    protected void paint(Graphics graphics)
    {
       //gfx.process(graphics, _objects, _score, ((Hero)_objects.elementAt(0)).getLives());
       //graphics.drawRect(_rectx, 10, 100, 100);
       graphics.setColor(0x00000000);
       
       graphics.fillRect(0, 0, _maxwidth, _maxheight);
       int yoffset = 2;
       int keyoff = 15;
       
      // Status
      graphics.drawText (_lastaction, 10,10, 0, _maxwidth-10);
      //graphics.drawText (String.valueOf(_lastkeystatus), 10,40, 0, _maxwidth-10);

      
       if (_lightkey == true) {
            _keytime++;
            
            // Blue block
            if (_lastaction.compareTo("blue") == 0) {                
                graphics.drawBitmap(0, yoffset, _blockwidth, _blockheight, _bluehigh, 0, 0);
            } else {
                graphics.drawBitmap(0, yoffset, _blockwidth, _blockheight, _bluenorm, 0, 0);
            }
            
            // Red
            if (_lastaction.compareTo("red") == 0) {
                graphics.drawBitmap(_maxwidth/2, yoffset, _blockwidth, _blockheight, _redhigh, 0, 0);
            } else {
                graphics.drawBitmap(_maxwidth/2, yoffset, _blockwidth, _blockheight, _rednorm, 0, 0);
            }
            
            // Yellow
            if (_lastaction.compareTo("yellow") == 0) {
                graphics.drawBitmap(0, (_maxheight/2)+yoffset, _blockwidth, _blockheight, _yellowhigh, 0, 0);
            } else {
                graphics.drawBitmap(0, (_maxheight/2)+yoffset, _blockwidth, _blockheight, _yellownorm, 0, 0);
            }
                    
            // Green
            if (_lastaction.compareTo("green") == 0) {
                graphics.drawBitmap(_maxwidth/2, (_maxheight/2)+yoffset, _blockwidth, _blockheight, _greenhigh, 0, 0);            
            } else {
                graphics.drawBitmap(_maxwidth/2, (_maxheight/2)+yoffset, _blockwidth, _blockheight, _greennorm, 0, 0);            
            }
            
            if (_keytime > _maxkeytime) {
                _lightkey = false;
                _keytime = 0;
                //StopSound();
            }
        } else {
            // Blue block        
            graphics.drawBitmap(0, yoffset, _blockwidth, _blockheight, _bluenorm, 0, 0);
            
            // Red
            graphics.drawBitmap(_maxwidth/2, yoffset, _blockwidth, _blockheight, _rednorm, 0, 0);
            
            // Yellow
            graphics.drawBitmap(0, (_maxheight/2)+yoffset, _blockwidth, _blockheight, _yellownorm, 0, 0);
                    
            // Green
            graphics.drawBitmap(_maxwidth/2, (_maxheight/2)+yoffset, _blockwidth, _blockheight, _greennorm, 0, 0);            
        }
        
        if (_showkeys == true) {
            if (_userlost == false) {
                // Blue block        
                graphics.drawBitmap(0+keyoff, yoffset+keyoff, _blockwidth, _blockheight, _tkey, 0, 0);
                // Red
                graphics.drawBitmap((_maxwidth/2)+keyoff, yoffset+keyoff, _blockwidth, _blockheight, _ukey, 0, 0);
                // Yellow
                graphics.drawBitmap(0+keyoff, (_maxheight/2)+yoffset+keyoff, _blockwidth, _blockheight, _gkey, 0, 0);        
                // Green
                graphics.drawBitmap((_maxwidth/2)+keyoff, (_maxheight/2)+yoffset+keyoff, _blockwidth, _blockheight, _jkey, 0, 0);                    
            }
        }
        
        if (_userlost == true) {
            graphics.setColor(0x00000000);
            graphics.drawText ("Your score: " + String.valueOf(_userscore), 10,10, 0, _maxwidth-10);
            
            graphics.drawBitmap((_maxwidth/2) - (180/2), ((_maxheight/2)-(29/2))+yoffset, 180, 29, _gameover, 0, 0);            
        }
      
    }
    
    
    // The keyChar method is called by the event handler when a key is pressed.
    public boolean keyChar(char key, int status, int time)
    {
 
        boolean retVal = false;
            _lastkey = key;
            
 
        switch (key)
        {
            case 't':
                _lastaction = "blue";
                _lightkey = true;
                //StopSound();
                PlaySound(_lastaction);
                validateEntry(_lastaction);
                break;
            case 'u':
                _lastaction = "red";
                _lightkey = true;
                //StopSound();
                PlaySound(_lastaction);
                validateEntry(_lastaction);
                break;
            case 'g':
                _lastaction = "yellow";
                _lightkey = true;
                //StopSound();
                PlaySound(_lastaction);
                validateEntry(_lastaction);
                break;
            case 'j':
                _lastaction = "green";
                _lightkey = true;
                //StopSound();
                PlaySound(_lastaction);
                validateEntry(_lastaction);
                break;
            //case 'r':
                //_simon = new SimonThread();
                //_userlevel++;
                //break;
        
            // If escape is pressed, we set the game to inactive (quit)
            case Characters.ESCAPE:
                _active = false;
                retVal = true;
                break;
 
            // If the spacebar is pressed, we call the fire method of our
            // hero object, causing him to fire a photon
            case Characters.SPACE: 
                retVal = true;
                break;
 
            
            
 
            default:
               break;
        }
 
        return retVal;
    }

} 
