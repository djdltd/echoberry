/*
 * InstructionsScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package EchoBerry;


import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;


/**
 * 
 */
class InstructionsScreen extends MainScreen {
    
    private EditField _instructions;
    
    InstructionsScreen() {    
        
        setTitle(new LabelField("EchoBerry - Instructions", LabelField.USE_ALL_WIDTH));
        
        _instructions = new EditField("", "The T, G, U and J keys on your Blackberry keyboard correspond to the coloured buttons on the screen.\n\nOnce the game has started the coloured buttons on the screen will light up in a given sequence. Your task is to repeat the same sequence.\n\nIf you repeat the sequence correctly you will be shown a longer sequence.\n\nKeep repeating the sequence EchoBerry shows you to get a higher score.\n\nIf you do not repeat the sequence exactly the game is over.\n\n");
        
        add(_instructions);
    }
    
        /**
        * Prevent the save dialog from being displayed.
        * 
        * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
        */
    public boolean onSavePrompt()
    {
        return true;
    }    

    
    /**
    * Close application
    * 
    * @see net.rim.device.api.ui.Screen#close() 
    */
    public void close()
    {
        //_listener.stop();
        //_sender.stop();
        
        super.close();
        
    }
} 
