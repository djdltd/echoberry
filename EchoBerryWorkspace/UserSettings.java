/*
 * UserSettings.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package EchoBerry;

import net.rim.device.api.util.Persistable;


/**
 * 
 */
class UserSettings implements Persistable {
    
    public String _usekeyprompt = "yes";
    public String _usesound = "yes";
    public int _startinglevel = 1;
    public boolean _registered = false;
    
    UserSettings() {    
    
    }
} 
