/*
 * Menu.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package EchoBerry;


import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;

/**
 * 
 */
class Menu extends MainScreen {
    
    GamePlay _game;  // GamePlay handles the action part of the game itself
    int _invokeID; // A handle to our invocation of scanning for when the game ends
    LabelField _description;
    private ObjectChoiceField _usekeyprompt;
    private ObjectChoiceField _usesound;
    String[] stryesno = new String[2];
    
    private UserSettings _settings = new UserSettings();
    private PersistentObject _persist;
    
    private MenuItem _aboutMenuItem = new MenuItem("About", 100, 10) 
    {                
        public void run()
        {                                    
            Dialog.inform("EchoBerry v1.3.\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");                                       
        }
    };
    
    private MenuItem _helpMenuItem = new MenuItem("Instructions", 100, 10) 
    {                
        public void run()
        {                          
            
            UiApplication.getUiApplication().pushScreen(new InstructionsScreen());
        }
    };
    
    ButtonField _startButton = new ButtonField("Start Game!", ButtonField.FIELD_HCENTER | ButtonField.FIELD_BOTTOM)
    {
      
        protected boolean trackwheelClick(int status, int time)
       {
          // If the button is pressed, we create a new GamePlay object
          setLocalsettings();
          _persist.commit();
          
          _game = new GamePlay(_settings);
 

          getUiEngine().pushScreen(_game);
 

          _invokeID = getApplication().invokeLater(new Runnable()
            {
                public void run()
                {
                    // Check to see if the game is done.
                    if (_game.getActive() == false)
                    {
                        // Cancel invoking this piece of code again (normally is invoked
                        // every 500 ms, as specified below)
                        getApplication().cancelInvokeLater(_invokeID);
 
                        // Kill the music
                        //GamePlay.snd.stopMusic();
 
                        // Pop the gameplay screen off the stack, which returns
                        // the user to the main menu
                        getUiEngine().popScreen(_game);
 
                        // Display the final score
                        //Dialog.inform("Game Finished. ");
 
                        // We're done with our game object now
                        //_game = null;
                    }
                    
                    //Dialog.alert("Start Game!");
                }
            }
            , 500,true); // rerun this code every 500ms
 
          return true;
       }        
    };

    public boolean isRegistered()
    {
        return _settings._registered;
    }

    public void setRegistered (boolean registered)
    {
        _settings._registered = registered;
        _persist.commit();
    }

    public void setLocalsettings()
    {   
        _settings._usekeyprompt = getMultichoice(_usekeyprompt, stryesno);
        _settings._usesound = getMultichoice(_usesound, stryesno);
    }
    
    public void setLocalform()
    {
           setMultichoice(_usekeyprompt, _settings._usekeyprompt, stryesno);
           setMultichoice(_usesound, _settings._usesound, stryesno);
    }
    
    public void setMultichoice(ObjectChoiceField myfield, String strChoice, String[] mychoices)
    {
        if (strChoice.trim() == "") {
            myfield.setSelectedIndex(0);
        } else {
            
            int c = 0;
            String strcur = "";
            
            for(c=0;c<mychoices.length;c++)
            {
                strcur = mychoices[c];
                if (strcur.compareTo(strChoice) == 0) {
                    myfield.setSelectedIndex(c);
                    return;
                }
            }            
        }
    }
    
    public String getMultichoice (ObjectChoiceField myfield, String[] mychoices)
    {
        int index = myfield.getSelectedIndex();
        
        return mychoices[index];
    }
    
    
    
    Menu() {        

        //super(NO_VERTICAL_SCROLL);

        // We set the title on the screen
        LabelField title = new LabelField("Welcome to EchoBerry", LabelField.FIELD_HCENTER);
        _description = new LabelField("Repeat what EchoBerry says for as long as your memory can endure. The longer you survive, the higher your score.");

        stryesno[0] = "yes";
        stryesno[1] = "no";
        
        _usekeyprompt = new ObjectChoiceField("Show key prompts: ", stryesno);
        _usesound = new ObjectChoiceField("Play sounds: ", stryesno);

        setTitle(title);



        // add our button that has the click method overridden
        add(_description);
        add(new SeparatorField());
        add(_usekeyprompt);
        add(_usesound);
        add(new SeparatorField());
        add(_startButton);
        addMenuItem(_helpMenuItem);
        addMenuItem(_aboutMenuItem);

        
        loadSettings();
        setLocalform(); 
        
        RegCheck();
    }
    
    public void RegCheck()
    {     
            UiApplication.getUiApplication().invokeLater( new Runnable()
            {
                public void run ()
                {
                    
                    if (_settings._registered == false) {
                        
                        
                        UiApplication.getUiApplication().pushModalScreen(new PasswordScreen());
                        
                        
                        //if (_regscreen.isPasswordok() == true) {
                            setRegistered(true);
                        //}
                    }

                }
            });
    }
    
    public boolean onSave()
    {
        setLocalsettings();
        _persist.commit();
        return true;
    }
    
    public void loadSettings ()
    {
         //45cf0646d22e96111ec8de504da94a49 // echoberry
         long KEY =  0x45cf0646d22e9611L;
         _persist = PersistentStore.getPersistentObject( KEY );
         _settings = (UserSettings) _persist.getContents();
         if( _settings == null ) {
             _settings = new UserSettings();
             _persist.setContents( _settings );
             //persist.commit()
         }
    }
} 
