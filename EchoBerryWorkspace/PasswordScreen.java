/*
 * PasswordScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package EchoBerry;

import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;


    public class PasswordScreen extends MainScreen
    {
        private boolean _passwordok = false;
        
        private MenuItem _passwordMenuItem = new MenuItem("Enter Registration Code", 100, 10) 
        {                
            public void run()
            {
                passwordPrompt();                   
            }
        };
        
        private MenuItem _forgotMenuItem = new MenuItem("Exit", 100, 10) 
        {                
            public void run()
            {
                System.exit(0);
            }
        };
        
        // Constructor
        public PasswordScreen()
        {
            setTitle(new LabelField("EchoBerry - Licensing", LabelField.USE_ALL_WIDTH));
            add(new LabelField("Please enter the Registration code you were given at purchase."));
                    
            addMenuItem(_passwordMenuItem);
            addMenuItem(_forgotMenuItem);
            //UiApplication.getUiApplication().pushScreen(popup);
            passwordPrompt();                        
        }

        public boolean isPasswordok()
        {
            return _passwordok;
        }

        public void passwordPrompt()
        {            
            UiApplication.getUiApplication().invokeLater( new Runnable()
            {
                public void run ()
                {

                    String strCode  = "";
                    PasswordPopupScreen popup = new PasswordPopupScreen(false);
                    
                    UiApplication.getUiApplication().pushModalScreen(popup);
                    
                    if (popup.getEscaped() == false) {
                        strCode = popup.getPassword();
                        
                        if (strCode.compareTo("cs77147023es1") == 0) {
                            _passwordok = true;
                            UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());    
                        } else {
                            Dialog.alert("Invalid Registration Code.");
                        }
                    } else {
                        Dialog.alert("You must enter your registration code to proceed.");
                    }
                }
            } );
        }
        
        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            //super.close();
            
        }
    }
